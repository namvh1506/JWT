package com.example.jwt.Service;

import com.example.jwt.Entity.User;

import java.util.List;

public interface UserService {
    User findByUsername(String username);

    List<User> findAll();
}
