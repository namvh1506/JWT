package com.example.jwt.Controller;

import com.example.jwt.Entity.User;
import com.example.jwt.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/find/{username}")
    public User getUser(@PathVariable String username) {
        return userService.findByUsername(username);
    }


    @GetMapping("/find")
    public List<User> findAll() {
        return userService.findAll();
    }
}
